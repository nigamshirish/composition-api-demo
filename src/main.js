import Vue from 'vue'
import '@/assets/tailwind.css'
import '@fortawesome/fontawesome-free/css/all.css'
import App from './App.vue'
import VueCompositionApi from '@vue/composition-api'

Vue.use(VueCompositionApi)

Vue.config.productionTip = false

import axios from 'axios'
axios.defaults.baseURL = 'https://api.thecatapi.com/v1'
axios.defaults.headers.common['x-api-key'] =
  '98a67728-d9c1-4e60-b350-0068f53810ea'

Vue.prototype.$axios = axios

new Vue({
  render: h => h(App)
}).$mount('#app')
